package main

import (
	"fmt"
	"os"
	"os/signal"

	"github.com/Shopify/sarama"
	mqtt "github.com/eclipse/paho.mqtt.golang"
)

func main() {
	data_ch := make(chan mqtt.Message)
	opts := mqtt.NewClientOptions().AddBroker("tcp://192.168.21.171:1883").SetClientID("sample")
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Partitioner = sarama.NewRandomPartitioner

	producer, err := sarama.NewSyncProducer([]string{"192.168.21.171:9092"}, config)
	defer producer.Close()

	if err != nil {
		panic(err)
	}
	//opts.SetProtocolVersion(4)

	c := mqtt.NewClient(opts)
	//defer c.Close()
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	//defer c.Disconnect(250)
	// trap SIGINT to trigger a shutdown.
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)

	for i := 0; i <10; i++ {

		go func() {
			fmt.Println("just test")
			for {
				if token := c.Subscribe("temperature", 0, func(client mqtt.Client, msg mqtt.Message) {
					data_ch <- msg

				}); token.Wait() && token.Error() != nil {
					fmt.Println(token.Error())
				}

			}
		}()
	}

	for i := 0; i < 10; i++ {
		go func(cnt int) {
			var message mqtt.Message
			//fmt.Println("In routine %d", cnt)
			msg := &sarama.ProducerMessage{
				Topic:     "mqtt-temperature",
				Partition: int32(-1),
				Key:       sarama.StringEncoder("key"),
			}
			for {
				message = <-data_ch
				fmt.Println("In routine %d", cnt)
				fmt.Printf("Received message on topic: %s\nMessage: %s\n", message.Topic(), message.Payload())
				msg.Value = sarama.ByteEncoder(message.Payload())
				paritition, offset, err := producer.SendMessage(msg)
				if err != nil {
					fmt.Println("Send Message Fail")
					panic(err)
				}

				fmt.Printf("Partion = %d, offset = %d\n", paritition, offset)
			}
		}(i)

	}

	select {
	case <-signals:
		return
	}

}
